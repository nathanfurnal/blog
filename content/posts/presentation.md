+++
title = "First blog post: an introduction"
date = 2021-12-27
tags = ["introduction"]
categories = ["misc"]
draft = false
+++

This is the first post to populate my blog, which was built with [Hugo](https://gohugo.io/). The blog
is held together in an [org-mode](https://orgmode.org/) file and exported with [ox-hugo](https://ox-hugo.scripter.co/), a backend that
allows for exporting `org` files to a Hugo compatible markdown.


## Presentation {#presentation}

I'm an avid Emacs user, it has been my main driver for around three years
now. My `init` file can be found [at this address](https://gitlab.com/nathanfurnal/dotemacs), it's a melting pot of programming
and writing utilities.

As far as programming goes, I've been mainly using Python and Java but I like to
dabble with Lisps and functional languages.

```emacs-lisp { linenos=table, linenostart=1 }
(defun hello ()
  (print "Hello Blog!"))
(hello)
```

```text
Hello Blog!
```